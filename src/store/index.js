import { createStore } from "vuex";
import projects from "./projects";
export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    projects,
  },
});
