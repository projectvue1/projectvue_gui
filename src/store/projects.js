export default {
  namespaced: true,
  state: {
    projectList: [],
    departments: [],
    taskList: [],
    projectComments: [],
    taskComments: [],
    project: {},
  },
  getters: {
    allTasks(state) {
      const tasks = [];
      for (let i = 0; i <= state.departments.length - 1; i++) {
        for (let x = 0; x <= state.departments[i].tasks.length - 1; x++) {
          tasks.push(state.departments[i].tasks[x]);
        }
      }
      return tasks;
    },
  },
  mutations: {
    setProjects(state, projects) {
      state.projectList = projects;
    },
    setProject(state, project) {
      state.project = project;
    },
    setDepartments(state, departments) {
      state.departments = departments;
    },
  },
  actions: {
    loadProject({ commit, dispatch }, projectId) {
      fetch("http://localhost:3333/api/v1/projects/" + projectId, {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          commit("setProject", data);
          dispatch("getDepartments");
        });
    },
    getProjects({ commit }) {
      fetch("http://localhost:3333/api/v1/projects", {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          commit("setProjects", data);
        });
    },
    createProject({ dispatch }, project) {
      fetch("http://localhost:3333/api/v1/projects", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(project),
      })
        .then((res) => res.json())
        .then(() => {
          dispatch("getProjects");
        });
    },
    getDepartments({ commit, state }) {
      commit("setDepartments", []);
      // eslint-disable-next-line no-prototype-builtins
      if (!state.project.hasOwnProperty("_id")) {
        return;
      }
      try {
        fetch("http://localhost:3333/api/v1/departments/" + state.project._id, {
          method: "GET",
          headers: {
            Accept: "application/json",
          },
        })
          .then((res) => res.json())
          .then((data) => {
            commit("setDepartments", data);
          });
      } catch (e) {
        console.log("Error fetching departments:", e)
      }
    },
    addDepartment({ dispatch, state }, department) {
      fetch("http://localhost:3333/api/v1/departments/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify({
          department: department,
          projectId: state.project._id,
        }),
      })
        .then((res) => res.json())
        .then(() => {
          dispatch("loadProject", state.project._id);
        });
    },
    addTask({ dispatch, state }, task) {
      fetch(
        "http://localhost:3333/api/v1/project/department/tasks/" +
          state.project._id,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
          body: JSON.stringify({
            ...task,
          }),
        }
      )
        .then((res) => res.json())
        .then(() => {
          dispatch("loadProject", state.project._id);
        });
    },
  },
};
