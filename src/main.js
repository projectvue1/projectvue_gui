import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import PrimeVue from 'primevue/config';
import markdownIt from "markdown-it";
import markdownItMermaid from "@liradb2000/markdown-it-mermaid";

import "./assets/tailwind.css";
const mdi = markdownIt();
mdi.use(markdownItMermaid);

const app = createApp(App);

import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primevue/resources/themes/bootstrap4-light-blue/theme.css';

app.use(PrimeVue, { ripple: true });
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import Card from "primevue/card";
app.component("DataTable", DataTable);
app.component("Column", Column);
app.component("Card", Card);
//Makes mdi a global variable available through the whole Vue App
//Use this.$mdi to access it in any component
app.config.globalProperties.$mdi = mdi;

app.use(store).use(router).mount("#app");
